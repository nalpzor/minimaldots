" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

if empty(glob('~/minimaldots/vim/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/minimaldots/vim/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/minimaldots/vim/.vim/plugged')

"Plug 'SirVer/ultisnips'

Plug 'https://github.com/itchyny/lightline.vim.git'

Plug 'https://github.com/airblade/vim-gitgutter.git'

Plug 'https://github.com/junegunn/goyo.vim.git'
Plug 'https://github.com/junegunn/limelight.vim.git'

Plug 'https://github.com/frazrepo/vim-rainbow.git'

Plug 'https://github.com/mhinz/vim-startify.git'

Plug 'https://github.com/preservim/nerdcommenter.git'
Plug 'https://github.com/preservim/nerdtree.git'

Plug 'https://github.com/dracula/vim.git',{'as':'dracula'}

Plug 'https://github.com/dense-analysis/ale.git'

Plug 'https://github.com/vimwiki/vimwiki.git'

call plug#end()

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif


" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd         " Show (partial) command in status line.
set showmatch		    " Show matching brackets.
set ignorecase	    " Do case insensitive matching
set smartcase		    " Do smart case matching
set incsearch		    " Incremental search
set ruler		        " Set cursor position
"set autowrite		  " Automatically save before commands like :next and :make
"set hidden		      " Hide buffers when they are abandoned
"set mouse=a		    " Enable mouse usage (all modes)
"
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set bs=indent,eol,start

"set list
set cursorline  " Set current line
"set cursorcolumn " Set current column
set number		      " Set line numbers
set relativenumber  "Set number relative to the line you are
set wildmenu
"set wildmode=longtst:full
set autoindent


set updatetime=500
" How searching behaves
set smartcase "
set incsearch " Search as you type

set laststatus=2

let g:rainbow_active = 1

let mapleader = ","

colorscheme dracula

    function! s:goyo_enter()
"      silent !tmux set status off
      "silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
      "set noshowmode
      "set noshowcmd
      "set scrolloff=999
      Limelight
      " ...
    endfunction

    function! s:goyo_leave()
"      silent !tmux set status on
      "silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
      "set showmode
      "set showcmd
"      set scrolloff=5
      Limelight!
      " ...
    endfunction

    autocmd! User GoyoEnter nested call <SID>goyo_enter()
    autocmd! User GoyoLeave nested call <SID>goyo_leave()

"set listchars=tab:\|\ 
"set list
" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

