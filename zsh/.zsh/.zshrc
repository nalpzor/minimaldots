# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
#zstyle :compinstall filename '/home/knalpa/.zshrc'

autoload -Uz compinit vcs_info promptinit
compinit
promptinit
precmd() { vcs_info }
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000
setopt autocd beep extendedglob nomatch notify globdots
bindkey -v
# End of lines configured by zsh-newuser-install
#
# No duplicate history
setopt HIST_SAVE_NO_DUPS PROMPT_SUBST

NEWLINE=$'\n'
export PROMPT='%F{green}%n%f@%F{magenta}%d%f ${NEWLINE}> '
export RPROMPT='${vcs_info_msg_0_} %F{red}%t%f'
#prompt suse
#

# Aliases
alias ll="ls -la"

# Git aliases
alias gtst="git status -sb"
alias gtlg="git log --oneline"
alias gtrv="git remote -v"
alias rgh="rg --hidden"
